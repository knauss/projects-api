#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-FileCopyrightText: 2016-2017 Harald Sitter <sitter@kde.org>
# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

require 'net/ssh'

bin = 'projects-api'
service = 'projects-kde-org-api'
user = 'projectsadmin'
host = 'hepta.kde.org' # 185.85.125.73
home = "/home/#{user}"
path = "#{home}/api/"
binpath = "#{path}/bin/"
systemd = "#{home}/.config/systemd/user"

system('go', 'build', '-v', '-o', bin) || raise

system('ssh', "#{user}@#{host}",
       "mkdir -p #{path}") || raise

system('rsync', '-av', '--progress',
       '-e', 'ssh', bin, "#{user}@#{host}:#{binpath}") || raise

system('ssh', "#{user}@#{host}",
       "mkdir -p #{home}/.config/systemd/user") || raise
system('rsync', '-av', '--progress',
       '-e', 'ssh', "systemd/#{service}.service",
       "#{user}@#{host}:#{systemd}") || raise
system('rsync', '-av', '--progress',
       '-e', 'ssh', "systemd/#{service}.socket",
       "#{user}@#{host}:#{systemd}") || raise
system('ssh', "#{user}@#{host}",
       'systemctl --user daemon-reload') || raise
system('ssh', "#{user}@#{host}",
       "systemctl --user stop #{service}.service") || raise
system('ssh', "#{user}@#{host}",
       "systemctl --user restart #{service}.socket") || raise

# optional, exit if docs fail
system('make', 'doc') || exit

system('rsync', '-avz', '--progress',
       '-e', 'ssh', 'doc', "#{user}@#{host}:#{path}/")

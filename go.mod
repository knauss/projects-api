module invent.kde.org/sysadmin/projects-api.git

go 1.14

require (
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964
	github.com/gin-gonic/gin v1.6.3
	github.com/stretchr/testify v1.5.1
	gopkg.in/yaml.v2 v2.2.8
)
